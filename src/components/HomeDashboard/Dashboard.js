
// loNE5rm11GSLN9-pc2ifgSoZj6ZHQlFwy4LLbHxPmypRsZdUcUJiJAzKxmP8nkexj-E TOKEN
/**
 * @vue-prop {Number} initialCounter - Initial counter's value
 * @vue-prop {Number} [step=1] - Step
 * @vue-data {Number} counter - Current counter's value
 * @vue-computed {String} message
 * @vue-event {Number} increment - Emit counter's value after increment
 * @vue-event {Number} decrement - Emit counter's value after decrement
 */



///https://web.copiloto.ai/api/rawdata?vehicles=891,957&from=2021-10-05&to=2021-10-09&fields=ecu_state_of_charge,speed_as_string:@speed__str,speed&speed=kph
//https://web.copiloto.ai/api/rawdata?groups=114&duration=P1D&fields=$basic,aid,ecu_state_of_charge&resample=event_time&freq=1D&group_by=aid TOA LA WEA
//https://web.copiloto.ai/api/rawdata?groups=114&duration=P1D&fields=$basic,ecu_state_of_charge,ecu_accumulated_charge,aid&resample=event_time&how=ecu_accumulated_charge:first,ecu_state_of_charge:first,mph:max,vid:first&freq=1D&group_by=aid
import axios from "axios";

export default {
  name: "Dashboard",
  data() {
    return {
      grupoFlota: 114,
      info: [],
      vehiculos: [],
      datosVehiculos: [],
      imeis: [867698041072011, 867698041054134],
      token: "d633d4392dc87686d4832ce007995558a9b061c96b2894467a3b4822",
      ids: [891, 957]
    };
  },
  methods: {
    // async getToken(){
    //   try{
    //     const token = await axios({
    //       method: "post",
    //       url: "https://web.copiloto.ai/api/login",
    //       headers: {
    //         "content-type": "application/json"
    //       },
    //       data: {
    //         username: "cristian.huenante@etrans.cl",
    //         password: "etrans2021!-",
    //       },
    //     });
    //     let tokencito = token.data.auth
    //     console.log(tokencito)
    //     return tokencito
    //   }catch(error){
    //     console.log(error)
    //   }
    // },
    async traerVehiculo(id) {
      try {
        return await axios({
          method: "get",
          url: 'https://web.copiloto.ai/api/vehicles',
          headers: {
            "Authenticate": this.token
          },
          params: {
            "ids": id
          }
        });
      } catch (error) {
        console.log(error)
      }
    },
    async traerDatosGrupos(id) {

      try {
        return await axios.get('https://web.copiloto.ai/api/groups/' + id, {
          headers: {
            "Authenticate": this.token
          }
        })
      } catch (error) {
        console.log(error)
      }
    },
    async datoSOC() {
      try {
        return await axios.get('https://web.copiloto.ai/api/rawdata?groups=' + this.grupoFlota + '&duration=P1D&fields=$basic,ecu_bms2_higher_voltage_cell,ecu_bms2_lower_voltage_cell,speed,io_pwr,ecu_bms2_batt_max_temp,ecu_bms2_batt_min_temp,ecu_bms5_charger_connected,aid,ecu_state_of_charge&resample=event_time&freq=1D&group_by=aid', {
          headers: {
            "Authenticate": this.token
          }
        })
      } catch (error) {
        console.log(error)
      }
    },
    async datoOnline(imei) {
      try {
        return await axios.get('https://web.copiloto.ai/api/devices/' + imei, {
          headers: {
            "Authenticate": this.token
          }
        })
      } catch (error) {
        console.log(error)
      }
    },
    armandoObjeto() {
      this.traerDatosGrupos(this.grupoFlota).then(respuesta => {
        let vehiculosGrupo = respuesta.data.vehicles
        let datosID = ""
        vehiculosGrupo.map((ids, index) => {
          vehiculosGrupo.length == index + 1 ? datosID += ids : datosID += ids + ","
        })
        this.traerVehiculo(datosID).then(res => {
          let datosVehiculosFlota = res.data.data
          this.datoSOC().then(repuestaSOC => {
            datosVehiculosFlota.map((vehiculos, indexVehiculo) => {
              repuestaSOC.data.events.map((eventos, indexEvento) => {
                if (indexVehiculo == indexEvento) {
                  vehiculos.estado = eventos.ecu_state_of_charge
                  vehiculos.velocidad = eventos.ecu_speed
                  vehiculos.temp_max_bateria = eventos.ecu_bms2_batt_max_temp
                  vehiculos.temp_min_bateria = eventos.ecu_bms2_batt_min_temp
                  vehiculos.voltage_max_celdas = eventos.ecu_bms2_higher_voltage_cell
                  vehiculos.voltage_min_celdas = eventos.ecu_bms2_lower_voltage_cell
                  this.datoOnline(datosVehiculosFlota[indexVehiculo].device).then(resOnline => {
                    //vehiculos.estadoOnline = resOnline.data.connection.online
                    if (resOnline.data.connection.online) {
                      vehiculos.isOnline = "ONLINE"
                    }
                    else {
                      vehiculos.isOnline = "OFFLINE"
                    }
                  })
                  console.log(datosVehiculosFlota[indexVehiculo])
                  if (eventos.ecu_bms5_charger_connected == 0.0) {
                    vehiculos.estado_cargador = "NO CARGANDO"
                  }
                  else {
                    vehiculos.estado_cargador = "CARGANDO"
                  }
                }
              })
            })
            this.vehiculos = datosVehiculosFlota
            console.log('VEHICULO', this.vehiculos)
          })
        })
      })
    }

  },
  created() {
    // this.datosPrueba()


    this.armandoObjeto()
    //setTimeout(this.$forceUpdate(), 5000)
  },
};