
import Axios from 'axios'
export default {
  data () {
    return {
      appointments: {},
      event_counter: 0,
      user: 'test'
    }
  },
  methods: {
    getUser: function () {
      return Axios
        .get('http://127.0.0.1:5000/users/get_user')
        .then(response => {
          this.user = response.data.username
          return this.user
        })
    },
    getAppointments: function () {
      this.getUser()
        .then(data => {
          let url = 'http://127.0.0.1:5000/appointments/get_appointments?user=' + data
          Axios
            .get(url)
            .then(response => { this.appointments = response.data })
        })
    },
    fetchData: function () {
      setInterval(() => {
        this.getAppointments()
      }, 150000)
    }
  },
  mounted () {
    this.fetchData()
  },
  created () {
    this.getAppointments()
  }
}